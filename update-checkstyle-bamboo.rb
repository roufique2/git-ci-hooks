#!/usr/bin/env ruby

# git update hook for asserting the ref being merged into a protected ref (e.g. master) 
# does not introduce an increase in checkstyle violations
#
# requires Ruby 1.9.3+ 

require_relative 'ci-util'
require 'rexml/document'

include REXML

# This example 
def count_checkstyle_violations(bamboo, commit)
    # grab the checkstyle.xml artifact from the build (assumes a shared artifact named 
    # 'checkstyle' with 'checkstyle-result.xml' at the root)
    checkstyle_xml = shared_artifact_for_commit(bamboo, commit, bamboo["checkstyle_key"], "checkstyle/checkstyle-result.xml")
    doc = Document.new checkstyle_xml
    # could go to town on the comparison here - but let's just count the raw number of
    # errors for the time being
    XPath.match(doc, "//error").length
end

# parse args supplied by git: <ref_name> <old_sha> <new_sha>
ref = simple_branch_name ARGV[0]
prevCommit = ARGV[1]
newCommit = ARGV[2]

# test if the updated ref is one we want to enforce green builds for
exit_if_not_protected_ref(ref)

# get the tip of the most recently merged branch
tip_of_merged_branch = find_newest_non_merge_commit(prevCommit, newCommit)

# parse our bamboo server config
bamboo = read_config("bamboo", ["url", "username", "password", "checkstyle_key"])

# calculate number of checkstyle violations for the old and new commits
prev_violations = count_checkstyle_violations(bamboo, prevCommit)
new_violations = count_checkstyle_violations(bamboo, tip_of_merged_branch)

# if the number of checkstyle violations has increased, block the update
if prev_violations > new_violations
    abort "#{shortSha(tip_of_merged_branch)} has #{new_violations} checkstyle violations! #{ref} currently has only #{prev_violations}." 
else 
    # if the number of checkstyle violations has decreased, kudos to the dev
    puts "Nice work! #{ref} has #{new_violations - prev_violations} fewer checkstyle violations than before."
end
